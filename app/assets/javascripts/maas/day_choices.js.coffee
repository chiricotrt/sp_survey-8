# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  setAnswers()
  setDependencies()
  setRequired()
  drawDelayBar()

setAnswers = ->
  $(".form-group").hide()
  $("#day-question").show()
  $("#change-plans").show()
  $("#extend-duration").show()


selectionMap =
  change_plans:
    "Yes": ["change-departure-time", "change-mode", "change-route", "attempted-previously"]
    "I cancel the trip": ["change-activity"]
  change_departure_time:
    "Yes": ["departure-time-choice"]
  change_mode:
    "Yes": ["mode-choice"]
  change_route:
    "Yes": ["route-factor"]
  change_activity:
    "Yes": ["activity-choice", "activity-duration", "activity-mode", "activity-departure-time"]

setDependencies = ->
  for id, map of selectionMap
    $("#" + id).on 'change', (event) ->
      console.log(event.target.id + " changed")
      selectedAnswer = $(event.target).children("option:selected").val()
      for answer, ids of selectionMap[event.target.id]
        if answer == selectedAnswer
          showDivs(ids)
        else
          hideDivs(ids)

showDivs = (a) ->
  for id in a
    console.log("showing #{id}")
    $("#" + id).show()
    key = id.replace(/-/g, '_')
    $("#" + key).prop("required", true)
    if key of selectionMap
      $("#" + key).trigger("change")

hideDivs = (a) ->
  for id in a
    console.log("hiding #{id}")
    $("#" + id).hide()
    key = id.replace(/-/g, '_')
    $("#" + key).prop("required", false)
    if key of selectionMap
      hideDivs(ids) for answer, ids of selectionMap[key]

setRequired = ->
  $("#change_plans").prop("required", true)
  $("#extend_duration").prop("required", true)

drawDelayBar = ->
  canvas = document.getElementById('canvas')
  W = canvas.width = canvas.offsetWidth
  H = canvas.height = canvas.offsetHeight
  H2 = H / 2.0
  ctx = canvas.getContext('2d')

  # draw bar
  x1 = 1.0 / 3.0 * W
  el = $("#sp_info")
  tt = parseInt(el.data("travel-time"))
  stt = parseInt(el.data("standard-travel-time"))
  dt = el.data("departure-time")
  f = tt / stt
  if tt <= stt
    ctx.fillStyle = "#687686"
  else
    ctx.fillStyle = "#687686"
  ctx.fillRect(0, H2, f * x1, H2)

  # text
  ctx.textAlign = "left"
  ctx.fillStyle = "black"
  ctx.font = "16px sans-serif"
  ctx.fillText(String(dt), 0.0, H2 - 5)
  timeStr = dt.split(":")
  hours = parseInt(timeStr[0])
  minutes = parseInt(timeStr[1])
  time = new Date()
  time.setHours(hours)
  time.setMinutes(minutes)
  standardArrivalTime = new Date()
  standardArrivalTime.setTime(time.getTime() + stt * 60 * 1000)
  standardArrivalTimeStr = String(standardArrivalTime.getHours()).padStart(2, '0') + ":" + String(standardArrivalTime.getMinutes()).padStart(2, '0')
  arrivalTime = new Date()
  arrivalTime.setTime(time.getTime() + tt * 60 * 1000)
  arrivalTimeStr = String(arrivalTime.getHours()).padStart(2, '0') + ":" + String(arrivalTime.getMinutes()).padStart(2, '0')

  ctx.textAlign = "center"
  ctx.fillText(standardArrivalTimeStr, x1, H2 - 5.0)
  ctx.fillText(arrivalTimeStr, f * x1, H2 - 5.0)

  # draw outline
  ctx.strokeRect(1.0, H2, W - 2.0, H2 - 1.0)

  # draw 1x travel time line
  ctx.beginPath()
  ctx.moveTo(x1, H2)
  ctx.lineTo(x1, H)
  ctx.stroke()
